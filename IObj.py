import json
from texttable import Texttable


# 天干
gan = '甲乙丙丁戊己庚辛壬癸'
# 地支
zhi = '子丑寅卯辰巳午未申酉戌亥'

# 六神和日期关系表
def get_sixgod_list(tg):
    """
    tg: 天干
    """
    l = ['青龙','朱雀','勾陈','腾蛇','白虎','玄武']
    index = 0
    if tg == '甲' or tg == '已':
        index = 0
    if tg == '丙' or tg == '丁':
        index = 1
    if tg == '戊' or tg == '己':
        index = 2
    if tg == '庚' or tg == '辛':
        index = 3
    if tg == '壬' or tg == '癸':
        index = 4
        
    return l[index:len(l)] + l[0:index]   
# 用神关系表
def get_god_map(wx):
    """
    wx 五行
    """
    god_map = {}
    if wx == '金':
        god_map['元神'] = '土'
        god_map['忌神'] = '火'
        god_map['仇神'] = '木'
    if wx == '土':
        god_map['元神'] = '火'
        god_map['忌神'] = '木'
        god_map['仇神'] = '水'
    if wx == '水':
        god_map['元神'] = '金'
        god_map['忌神'] = '土'
        god_map['仇神'] = '火'  
    if wx == '木':
        god_map['元神'] = '水'
        god_map['忌神'] = '金'
        god_map['仇神'] = '土'
    if wx == '火':
        god_map['元神'] = '木'
        god_map['忌神'] = '水'
        god_map['仇神'] = '金'
    
    return god_map


def getGua(g_id):
    f = open('najia.json','r')
    data = json.load(f)
    for g in data:
        if g['g_id'] == g_id:
            return g

def showGua(gua):
    # 卦象
        g_id = '%s%s%s%s%s%s' % (gua.y1.yao, gua.y2.yao, gua.y3.yao, gua.y4.yao, gua.y5.yao, gua.y6.yao)
        g = getGua(g_id)
  

class Gua(object):

    def __init__(self, n1, n2, n3, n4, n5, n6):
        # 初爻
        self.y1 = Yao(n1,1)
        self.y2 = Yao(n2,2)
        self.y3 = Yao(n3,3)
        self.y4 = Yao(n4,4)
        self.y5 = Yao(n5,5)
        self.y6 = Yao(n6,6)

        
        #起卦月地支
        self.pray_month_dz = ''
        #起卦月天干
        self.pray_month_tg=''
        #起卦日地支
        self.pray_day_dz = ''
        #起卦日天干
        self.pray_day_tg = '甲'
        g_id = '%s%s%s%s%s%s' % (self.y1.yao, self.y2.yao, self.y3.yao, self.y4.yao, self.y5.yao, self.y6.yao)
        g = getGua(g_id)
                #卦名
        self.gname = g['gname_all']
        self.gname_simple = g['gname_simple']
        #卦词
        self.detail = g['gua-detail']
#         从json对象到内存实例
        def najia(i,y):
            """
            纳甲
            """
            g_unit =  g['g_y'+str(i)]
            y.lq = g_unit['liuqin']
            y.tg = g_unit['tiangan']
            y.dz = g_unit['dizhi']
            y.wx = g_unit['wuxing']
            y.sy = g_unit['shiying']
            y.detail = g_unit['yaoci']
        najia(1,self.y1)
        najia(2,self.y2)
        najia(3,self.y3)
        najia(4,self.y4)
        najia(5,self.y5)
        najia(6,self.y6)
        # 用神的五行
        self.yswx = ''
    
    def getYao(self,num):
        """
        获取第几爻
        """
        y = None    
        if num == 1:
            y = self.y1
        if num == 2:
            y = self.y2
        if num == 3:
            y = self.y3
        if num == 4:
            y = self.y4
        if num == 5:
            y = self.y5
        if num == 6:
            y = self.y6
        return y
    
    
    def getChange(self):
        """
        获取变卦
        """
        def change(y):
            change = 0
            if y.bian == True and y.yao == 0:
                change = 7
            elif y.bian == True and y.yao == 1:
                change = 8
            elif y.bian == False and y.yao ==1:
                change = 7
            elif y.bian == False and y.yao == 0:
                change = 8
            return change
        
        c1 = change(self.y1)
        c2 = change(self.y2)
        c3 = change(self.y3)
        c4 = change(self.y4)
        c5 = change(self.y5)
        c6 = change(self.y6)
        return Gua(c1,c2,c3,c4,c5,c6)
    
    def set_pray_day(self,**tgdz):
        """
        设置起卦天干地支
        tg 天干
        dz 地支
        """
        tg=''
        dz=''
        if 'tg' in tgdz:
            tg = tgdz['tg']
        if 'dz' in tgdz:
            dz = tgdz['dz'] 
        if tg not in gan or dz not in zhi :
            print("请输入正确的天干地支")
            return
        self.pray_day_dz = dz
        self.pray_day_tg = tg
        
    def set_pray_month(self,**tgdz):
        """
        设置起卦天干地支
        tg 天干
        dz 地支
        """
        tg=''
        dz=''
        if 'tg' in tgdz:
            tg = tgdz['tg']
        if 'dz' in tgdz:
            dz = tgdz['dz'] 
        if tg not in gan or dz not in zhi :
            print("请输入正确的天干地支")
            return
        self.pray_month_dz = dz
        self.pray_month_tg = tg
    def set_use_god(self,num):
        """
        设置第几爻为用神，从下往上算，
        """
        for y in [self.y6,self.y5,self.y4,self.y3,self.y2,self.y1]:
            if y.id == num:
                y.ys = '用神'
                self.yswx = y.wx
         
            
    def __str__(self, *args, **kwargs):
        """
        重写tostring方法
        """
        #获取变卦
        print(self.gname)
        print(self.detail)
        print("起卦月  %s%s " % (self.pray_month_tg,self.pray_month_dz))
        print("起卦日  %s%s 日" % (self.pray_day_tg,self.pray_day_dz))
        change_g = self.getChange()
        
        content = ''
        
        table = Texttable()
        table.add_row(['六神','六亲','地支','五行','卦象','世应','变卦','用神'])
                 
        for y in [self.y6,self.y5,self.y4,self.y3,self.y2,self.y1]:
#             content +='%s%s%s ' %(y.lq,y.dz,y.wx)
            
            
            # 卦象
            gua_xiang = ''
            if y.yao == 1:
                gua_xiang = "▅▅▅▅▅▅"
            else:
                gua_xiang = "▅▅　▅▅"
            
            # 显示世应
            shiyin = '    '
            if len(y.sy) > 0:
                shiyin = y.sy
            
            # 显示变卦
            bianGua = ""
            change_y = change_g.getYao(y.id)
            if change_y.yao != y.yao:
                bianGua = "(%s)"%change_y.dz
                
                
            # 显示用神
            gdmap = get_god_map(self.yswx) 
            for k,v in gdmap.items():
                if v == y.wx:
                    y.ys = k
                    break
                
            # 排六神
            ls = get_sixgod_list(self.pray_day_tg)[y.id-1]
            
            table.add_row([ls, y.lq, y.dz, y.wx, gua_xiang, shiyin, bianGua, y.ys])
        print(table.draw() + "\n")
        return content
    
            
class Yao(object):
    "从初爻开始网上起卦，6为老阴，7为少阳，8为少阳，9为老阳"

    def __init__(self, num,id):
        """
        id 第几爻，最下面的为第一爻
        yao 阴爻还是阳爻 1是阳 0是阴
        bianGua是否是变卦
        lq 六亲
        ls 六神
        sy 世应
        ys 用神
        """
        self.id = id
         # 六亲
        self.lq = None
        #  六神
        self.ls = None
        #  世应
        self.sy = None
        # 用神
        self.ys = '    ' 
        # 天干
        self.tg = None
        # 地支
        self.dz = None
        # 五行
        self.wx = None
        # 爻词
        self.detail = None
        # 老阴
        if num == 6:
            self.yao = 0
            self.bian = True
        elif num == 7:
            self.yao = 1
            self.bian = False
        elif num == 8:
            self.yao = 0
            self.bian = False
        elif num == 9:
            self.yao = 1
            self.bian = True
        else:
            print("请输入正确的计数，抛掷三枚硬币 \n \
            正面向上为3，反面向上为2，输入三枚硬币的总计数")
            return
        
#         @property
#         def detail(self):
#             return self.detail
#     
#         @detail.setter
#         def detail(self, detail):
#                 self.detail = detail
#         
#                
#         @property
#         def lq(self):
#             return self.lq
#     
#         @lq.setter
#         def lq(self, lq):
#                 self.lq = lq
#         
#         @property
#         def ls(self):
#             return self.ls
#     
#         @ls.setter
#         def ls(self, ls):
#                 self.ls = ls
#         
#         @property
#         def sy(self):
#             return self.sy
#     
#         @sy.setter
#         def sy(self, sy):
#                 self.sy = sy
#                 
#         @property
#         def ys(self):
#             return self.ys
#     
#         @sy.setter
#         def ys(self, ys):
#                 self.ys = ys
#                 
#         @property
#         def tg(self):
#             return self.tg
#     
#         @tg.setter
#         def tg(self, tg):
#                 self.tg = tg
#                 
#         # 地支        
#         @property
#         def dz(self):
#             return self.dz
#     
#         @dz.setter
#         def dz(self, dz):
#                 self.dz = dz
#         
#         # 五行
#         @property
#         def wx(self):
#             return self.wx
#     
#         @wx.setter
#         def wx(self, wx):
#                 self.wx = wx
#         
#          # 索引
#         @property
#         def id(self):
#             return self.id
#     
#         @id.setter
#         def id(self, id):
#                 self.id = id
             
    def __str__(self, *args, **kwargs):
        yaoStr = "---------"
        if self.yao == 0:
            yaoStr = "---    ---"
        return yaoStr
